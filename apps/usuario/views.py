from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import UsuarioForm, CambiarContrasenaForm, ModificarUsuarioForm
from .models import UserSCATUAZ

# Create your views here.


@login_required
def lista_usuario(request):
    if request.user.is_superuser:
        usuarios = UserSCATUAZ.objects.all()
        context = {
            'actual': 'usuario',
            'usuarios': usuarios,
        }
        return render(request, 'usuario/lista_usuario.html', context)
    return redirect('trabajador:lista_trabajador')


@login_required
def agregar_usuario(request):
    if request.user.is_superuser:
        if request.method == 'POST':
            form = UsuarioForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('usuario:lista_usuario')
        else:
            form = UsuarioForm()
        return render(request, 'usuario/agregar_usuario.html', {'form': form})
    return redirect('trabajador:lista_trabajador')


@login_required
def eliminar_usuario(request, id):
    if request.user.is_superuser:
        if request.method == 'POST':
            usuario = UserSCATUAZ.objects.get(pk=id)
            usuario.delete()
            return redirect('usuario:lista_usuario')
        else:
            usuario = UserSCATUAZ.objects.get(pk=id)
            return render(request, 'usuario/confirm_delete_user.html',
                          {'usuario': usuario})
    return redirect('trabajador:lista_trabajador')


@login_required
def modificar_usuario(request, id):
    if request.user.is_superuser:
        usuario = UserSCATUAZ.objects.get(pk=id)
        if request.method == 'POST':
            form = ModificarUsuarioForm(request.POST, instance=usuario)
            if form.is_valid():
                form.save()
                return redirect('usuario:lista_usuario')
        else:
            form = ModificarUsuarioForm(instance=usuario)
        return render(
            request, 'usuario/modificar_usuario.html', {
                'form': form, 'usuario': usuario})
    return redirect('trabajador:lista_trabajador')


@login_required
def cambiar_contrasena(request, id):
    if request.user.is_superuser:
        usuario = UserSCATUAZ.objects.get(pk=id)
        if request.method == 'POST':
            form = CambiarContrasenaForm(usuario, data=request.POST)
            if form.is_valid():
                form.clean_new_password2()
                form.save()
                return redirect('usuario:lista_usuario')
        else:
            form = CambiarContrasenaForm(usuario)
        return render(request, 'usuario/cambiar_contrasena.html',
                      {'form': form, 'usuario': usuario})
    return redirect('trabajador:lista_trabajador')


@login_required
def ver_usuario(request, id):
    if request.user.is_superuser:
        usuario = UserSCATUAZ.objects.get(pk=id)
        context = {
            'usuario': usuario,
        }
        return render(request, 'usuario/ver_usuario.html', context)
    return redirect('trabajador:lista_trabajador')
