# Generated by Django 2.2.10 on 2020-10-10 18:11

import apps.trabajador.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trabajador', '0003_auto_20201004_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trabajador',
            name='grado_max_estudios',
            field=models.CharField(choices=[('0', 'Primaria'), ('1', 'Secundaria'), ('2', 'Bachillerato o Preparatoria'), ('3', 'Carrera Comercial'), ('4', 'Carrera Técnica'), ('5', 'Normal'), ('6', 'Normal Superior'), ('7', 'Licenciatura'), ('8', 'Pasante de Carrera Profesional'), ('9', 'Especialidad'), ('10', 'Maestría'), ('11', 'Doctorado'), ('12', 'Pasante de Maestría'), ('13', 'Candidato a Doctor'), ('14', 'Postdoctorado')], max_length=2, verbose_name='*Grado maximo de estudios'),
        ),
        migrations.AlterField(
            model_name='trabajador',
            name='sexo',
            field=models.CharField(choices=[('0', 'Masculino'), ('1', 'Femenino')], max_length=1, verbose_name='*Genero'),
        ),
        migrations.AlterField(
            model_name='trabajador',
            name='validado_renapo',
            field=models.CharField(choices=[('0', 'No'), ('1', 'Si')], max_length=1, null=True, verbose_name='Validado por Renapo'),
        ),
        migrations.AlterField(
            model_name='trabajador',
            name='validado_siri',
            field=models.CharField(choices=[('0', 'No'), ('1', 'Si')], max_length=1, null=True, verbose_name='Validado por Siri'),
        ),
        migrations.CreateModel(
            name='DocumentosPersonales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('acta_nacimiento', models.FileField(null=True, upload_to=apps.trabajador.models.get_docs_path, verbose_name='Acta de nacimiento')),
                ('comprobante_domicilio', models.FileField(null=True, upload_to=apps.trabajador.models.get_docs_path, verbose_name='Comprobante de domicilio')),
                ('curp_rfc', models.FileField(null=True, upload_to=apps.trabajador.models.get_docs_path, verbose_name='CURP/RFC')),
                ('sat', models.FileField(null=True, upload_to=apps.trabajador.models.get_docs_path, verbose_name='SAT')),
                ('ine', models.FileField(null=True, upload_to=apps.trabajador.models.get_docs_path, verbose_name='INE')),
                ('id_trabajador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trabajador.Trabajador')),
            ],
        ),
    ]
