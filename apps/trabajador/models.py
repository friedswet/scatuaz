from django.db import models
from django.core.validators import MinLengthValidator

from apps.usuario.models import UserSCATUAZ

OPCIONES_SEXO = (
    ('0', 'Masculino'),
    ('1', 'Femenino'),
)

OPCIONES_ESTUDIOS = (
    ('0', 'Primaria'),
    ('1', 'Secundaria'),
    ('2', 'Bachillerato o Preparatoria'),
    ('3', 'Carrera Comercial'),
    ('4', 'Carrera Técnica'),
    ('5', 'Normal'),
    ('6', 'Normal Superior'),
    ('7', 'Licenciatura'),
    ('8', 'Pasante de Carrera Profesional'),
    ('9', 'Especialidad'),
    ('10', 'Maestría'),
    ('11', 'Doctorado'),
    ('12', 'Pasante de Maestría'),
    ('13', 'Candidato a Doctor'),
    ('14', 'Postdoctorado'),
)

OPCIONES_BOOL = (
    ('0', 'No'),
    ('1', 'Si'),
)

def get_docs_path(instance, filename):
    return f'docs/trabajador/{instance.id_trabajador.rfc}/{filename}'

def get_docs_path_2(instance, filename):
    return f'docs/trabajador/{instance.id_doc_academicos.id_trabajador.rfc}/{filename}'

class Trabajador(models.Model):
    nombre = models.CharField('*Nombre(s)', max_length=100)
    paterno = models.CharField('*Primer apellido', max_length=100)
    materno = models.CharField('*Segundo apellido', max_length=100)
    rfc = models.CharField('*RFC', max_length=13, unique=True)
    curp = models.CharField('*CURP', max_length=18, unique=True)
    sexo = models.CharField('*Genero', max_length=1, choices=OPCIONES_SEXO)
    pais_residencia = models.CharField('*Pais de residencia', max_length=100)
    estado_residencia = models.CharField('*Estado de residencia', max_length=100)
    municipio_residencia = models.CharField('*Municipio de residencia', max_length=100)
    calle = models.CharField('*Calle', max_length=100)
    numero = models.CharField('*Numero', max_length=10)
    colonia = models.CharField('*Colonia', max_length=100)
    cp = models.CharField('*CP', max_length=6)
    telefono = models.CharField('*Telefono', max_length=10, unique=True)
    email = models.CharField('*Correo electronico', max_length=50, unique=True)
    matricula_administrativo = models.CharField('*Matricula administrativa', max_length=6, unique=True)
    matricula_gremial = models.CharField('*Matricula gremial', max_length=6, unique=True)
    grado_max_estudios = models.CharField('*Grado maximo de estudios', max_length=2, choices=OPCIONES_ESTUDIOS)
    nss = models.CharField('*Numero de seguro social (NSS)', max_length=11, unique=True)
    no_issste = models.CharField('*Numero de ISSSTE', max_length=50, unique=True)
    validado_renapo = models.CharField('Validado por Renapo', max_length=1, null=True, choices=OPCIONES_BOOL)
    validado_siri = models.CharField('Validado por Siri', max_length=1, null=True, choices=OPCIONES_BOOL)
    alta_usuario = models.ForeignKey(UserSCATUAZ, verbose_name="Usuario", null=True, on_delete=models.SET_NULL)
    alta_fecha = models.DateField(auto_now_add=True)

    def __str__(self):
        return (self.nombre + ' ' + self.paterno + ' ' + self.materno).strip()


class Actualizacion(models.Model):
    id_trabajador = models.ForeignKey(Trabajador, on_delete=models.CASCADE)
    usuario = models.ForeignKey(UserSCATUAZ, on_delete=models.CASCADE)
    fecha = models.DateField(auto_now_add=True)
    comentario = models.TextField('Comentario', max_length=500, null=True)
    

################ Documentos personales ################
class DocumentosPersonales(models.Model):
    id_trabajador = models.OneToOneField(Trabajador, on_delete=models.CASCADE, unique=True)
    acta_nacimiento = models.FileField('Acta de nacimiento', upload_to=get_docs_path, null=True)
    comprobante_domicilio = models.FileField('Comprobante de domicilio', upload_to=get_docs_path, null=True)
    curp_rfc = models.FileField('CURP/RFC', upload_to=get_docs_path, null=True)
    sat = models.FileField('SAT', upload_to=get_docs_path, null=True)
    ine = models.FileField('INE', upload_to=get_docs_path, null=True)


################ Documentos academicos ################
class DocumentosAcademicos(models.Model):
    id_trabajador = models.OneToOneField(Trabajador, on_delete=models.CASCADE, unique=True)
    primaria = models.FileField('Primaria', upload_to=get_docs_path, null=True)
    secundaria = models.FileField('Secundaria', upload_to=get_docs_path, null=True)
    preparatoria = models.FileField('Preparatoria', upload_to=get_docs_path, null=True)
    licenciatura = models.FileField('Licenciatura', upload_to=get_docs_path, null=True)
    cedula_profesional = models.FileField('Cédula profesional', upload_to=get_docs_path, null=True)

class DocumentosAcademicosConstancia(models.Model):
    id_doc_academicos = models.ForeignKey(DocumentosAcademicos, on_delete=models.CASCADE)
    nombre = models.CharField('Nombre de constancia', max_length=100)
    archivo = models.FileField('Constancia', upload_to=get_docs_path_2)

class DocumentosAcademicosDiploma(models.Model):
    id_doc_academicos = models.ForeignKey(DocumentosAcademicos, on_delete=models.CASCADE)
    nombre = models.CharField('Nombre de constancia', max_length=100)
    archivo = models.FileField('Diploma', upload_to=get_docs_path_2)

class DocumentosAcademicosReconocimiento(models.Model):
    id_doc_academicos = models.ForeignKey(DocumentosAcademicos, on_delete=models.CASCADE)
    nombre = models.CharField('Nombre de constancia', max_length=100)
    archivo = models.FileField('Reconocimiento', upload_to=get_docs_path_2)
