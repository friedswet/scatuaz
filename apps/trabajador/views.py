from django.shortcuts import render, redirect
from django.db.models import Q
from . import models
from .forms import TrabajadorForm
from apps.usuario.models import UserSCATUAZ
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib import messages
from django.urls import reverse_lazy
from django.http import JsonResponse

import os

trabajador_fields = [
    'nombre', 'paterno', 'materno', 'rfc', 'curp',
    'sexo', 'pais_residencia', 'estado_residencia',
    'municipio_residencia', 'calle', 'numero',
    'colonia', 'cp', 'telefono', 'email',
    'matricula_administrativo', 'matricula_gremial',
    'grado_max_estudios', 'nss', 'no_issste',
    'validado_renapo', 'validado_siri',
]

archivos_personales = [
    'acta_nacimiento', 'comprobante_domicilio', 'curp_rfc', 'sat', 'ine',
]

archivos_academicos = [
    'primaria', 'secundaria', 'preparatoria', 'licenciatura', 'cedula_profesional', 
]

nombre_archivos = {
    'acta_nacimiento': 'acta de nacimiento',
    'comprobante_domicilio': 'comprobante de domicilio',
    'curp_rfc': 'CURP/RFC',
    'sat': 'SAT',
    'ine': 'INE',
    'primaria': 'primaria',
    'secundaria': 'secundaria',
    'preparatoria': 'preparatoria',
    'licenciatura': 'licenciatura',
    'cedula_profesional': 'cédula profesional',
    'constancia': 'constancia',
    'diploma': 'diploma',
    'reconocimiento': 'reconocimiento'
}

def comparar_forms(form):
    campos_actualizados = ''
    for field in trabajador_fields:
        if field in form.changed_data:
            if campos_actualizados != '':
                campos_actualizados += ', '
            campos_actualizados += field
    return campos_actualizados


@login_required
def lista_trabajador(request):
    trabajadores = models.Trabajador.objects.all().order_by('id')
    query = request.GET.get('q')
    if query:
        trabajadores = models.Trabajador.objects.filter(
            Q(nombre=query) | Q(paterno=query) | Q(
                materno=query) | Q(rfc=query)
        ).distinct()

    paginator = Paginator(trabajadores, 10)
    numero_pagina = request.GET.get('page')

    try:
        pagina = paginator.page(numero_pagina)
    except PageNotAnInteger:
        pagina = paginator.page(1)
    except EmptyPage:
        pagina = paginator.page(paginator.num_pages)

    context = {
        'actual': 'trabajador',
        'pagina': pagina,
    }
    return render(request, 'trabajador/lista_trabajador.html', context)


@login_required
def ver_trabajador(request, id):
    trabajador = models.Trabajador.objects.get(pk=id)
    try:
        actualizaciones = models.Actualizacion.objects.filter(id_trabajador_id=id)
    except BaseException:
        actualizaciones = ()
    context = {
        'trabajador': trabajador,
        'actualizaciones': actualizaciones
    }

    return render(request, 'trabajador/ver_trabajador.html', context)


@login_required
def agregar_trabajador(request):
    if request.method == 'POST':
        form = TrabajadorForm(request.POST)
        if form.is_valid():
            tmp = form.save(commit=False)
            usuario = UserSCATUAZ.objects.get(pk=request.user.id)
            tmp.alta_usuario = usuario
            tmp.save()
            messages.success(request, 'El trabajador quedo guardado.')
            return redirect('trabajador:ver_trabajador', id=tmp.id)
        messages.error(request, 'Corrige los errores.')
    else:
        form = TrabajadorForm()
    context = {
        'form': form
    }
    return render(request, 'trabajador/agregar_trabajador.html', context)


@login_required
def modificar_trabajador(request, id):
    trabajador = models.Trabajador.objects.get(pk=id)
    if request.method == 'POST':
        form = TrabajadorForm(request.POST, instance=trabajador)
        if form.is_valid():
            tmp = form.save(commit=False)
            comentario = comparar_forms(form)
            models.Actualizacion.objects.create(
                id_trabajador=trabajador, usuario=request.user, comentario=comentario)
            messages.success(request, 'Los cambios quedaron guardados.')
            return redirect('trabajador:ver_trabajador', id=tmp.id)
        messages.error(request, 'Corrige los errores.')
    else:
        form = TrabajadorForm(instance=trabajador)
        context = {
            'form': form,
            'trabajador': trabajador
        }
    return render(request, 'trabajador/agregar_trabajador.html', context)


@login_required
def eliminar_trabajador(request, pk):
    if request.method == 'POST':
        try:
            trabajador = models.Trabajador.objects.get(pk=pk)
            trabajador.delete()
            messages.success(request, 'El trabajador quedo eliminado.')
            return redirect('trabajador:lista_trabajador')
        except models.Trabajador.DoesNotExist:
            trabajadores = models.Trabajador.objects.all()
            context = {
                'trabajadores': trabajadores
            }
            messages.error(request, 'El trabajador no pudo ser eliminado.')
            return render(request, 'trabajador/lista_trabajador.html', context)
    else:
        trabajador = models.Trabajador.objects.get(pk=pk)
        return render(request, 'trabajador/confirm_delete.html',
                      {'trabajador': trabajador})

@login_required
def ver_archivero(request, id):
    trabajador = models.Trabajador.objects.get(pk=id)
    context = {
        'trabajador': trabajador
    }

    doc_personales = models.DocumentosPersonales.objects.filter(id_trabajador__id=trabajador.id).first()
    if doc_personales:
        context['doc_personales'] = doc_personales

    doc_academicos = models.DocumentosAcademicos.objects.filter(id_trabajador__id=trabajador.id).first()
    if doc_academicos:
        context['doc_academicos'] = doc_academicos

        doc_aca_constancia = models.DocumentosAcademicosConstancia.objects.filter(id_doc_academicos_id=doc_academicos.id)
        if doc_aca_constancia:
            context['doc_aca_constancia'] = doc_aca_constancia

        doc_aca_diploma = models.DocumentosAcademicosDiploma.objects.filter(id_doc_academicos_id=doc_academicos.id)
        if doc_aca_diploma:
            context['doc_aca_diploma'] = doc_aca_diploma
        
        doc_aca_reconocimiento = models.DocumentosAcademicosReconocimiento.objects.filter(id_doc_academicos_id=doc_academicos.id)
        if doc_aca_reconocimiento:
            context['doc_aca_reconocimiento'] = doc_aca_reconocimiento

    return render(request, 'trabajador/ver_archivero.html', context)
    
def agregar_archivo(request, id, archivo):
    trabajador = models.Trabajador.objects.get(pk=id)
    context = {
        'trabajador': trabajador
    }
    if request.method == 'POST':
        instance = None
        if archivo in archivos_personales:
            doc_personales = models.DocumentosPersonales.objects.filter(id_trabajador_id=trabajador.id).first()
            if not doc_personales:
                instance = models.DocumentosPersonales(id_trabajador_id=trabajador.id)
                setattr(instance, archivo, request.FILES[archivo])
            else:
                instance = models.DocumentosPersonales.objects.filter(id_trabajador_id=trabajador.id).first()
                if getattr(instance, archivo) and os.path.isfile(getattr(instance, archivo).path):
                    os.remove(getattr(instance, archivo).path)
                setattr(instance, archivo, request.FILES[archivo])
        elif archivo in archivos_academicos:
            doc_academicos = models.DocumentosAcademicos.objects.filter(id_trabajador_id=trabajador.id).first()
            if not doc_academicos:
                instance = models.DocumentosAcademicos(id_trabajador_id=trabajador.id)
                setattr(instance, archivo, request.FILES[archivo])
            else:
                instance = models.DocumentosAcademicos.objects.filter(id_trabajador_id=trabajador.id).first()
                if getattr(instance, archivo) and os.path.isfile(getattr(instance, archivo).path):
                    os.remove(getattr(instance, archivo).path)
                setattr(instance, archivo, request.FILES[archivo])
        elif archivo == 'constancia':
            doc_academicos = models.DocumentosAcademicos.objects.filter(id_trabajador_id=trabajador.id).first()
            if not doc_academicos:
                doc_academicos = models.DocumentosAcademicos(id_trabajador_id=trabajador.id)
            instance = models.DocumentosAcademicosConstancia(id_doc_academicos_id=doc_academicos.id)
            setattr(instance, 'archivo', request.FILES[archivo])
            setattr(instance, 'nombre', request.POST['nombre'])
        elif archivo == 'diploma':
            doc_academicos = models.DocumentosAcademicos.objects.filter(id_trabajador_id=trabajador.id).first()
            if not doc_academicos:
                doc_academicos = models.DocumentosAcademicos(id_trabajador_id=trabajador.id)
            instance = models.DocumentosAcademicosDiploma(id_doc_academicos_id=doc_academicos.id)
            setattr(instance, 'archivo', request.FILES[archivo])
            setattr(instance, 'nombre', request.POST['nombre'])
        elif archivo == 'reconocimiento':
            doc_academicos = models.DocumentosAcademicos.objects.filter(id_trabajador_id=trabajador.id).first()
            if not doc_academicos:
                doc_academicos = models.DocumentosAcademicos(id_trabajador_id=trabajador.id)
            instance = models.DocumentosAcademicosReconocimiento(id_doc_academicos_id=doc_academicos.id)
            setattr(instance, 'archivo', request.FILES[archivo])
            setattr(instance, 'nombre', request.POST['nombre'])
        else:
            messages.error(request, 'No se selecciono un archivo.')
        if instance:
            instance.save()
            messages.success(request, 'El archivo quedo guardado.')
        else:
            messages.error(request, 'El archivo no pudo ser guardado.')
        return JsonResponse(data={'redirect':str(reverse_lazy('trabajador:ver_archivero', kwargs={'id':trabajador.id}))})
    else:
        if archivo in archivos_personales:
            doc_personales = models.DocumentosPersonales.objects.filter(id_trabajador_id=trabajador.id).first()
            if doc_personales and getattr(doc_personales, archivo):
                context['update'] = True
        if archivo in archivos_academicos:
            doc_academicos = models.DocumentosAcademicos.objects.filter(id_trabajador_id=trabajador.id).first()
            if doc_academicos and getattr(doc_academicos, archivo):
                context['update'] = True
        if archivo in ['constancia', 'diploma', 'reconocimiento']:
            context['incluir_nombre'] = True
        context['tipo_archivo'] = archivo
        context['nombre_archivo'] = nombre_archivos[archivo]
        return render(request, 'trabajador/modal_agregar_archivo.html', context)