from . import models
from django import forms


class FormControl(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormControl, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

class TrabajadorForm(FormControl):
    
    class Meta:
        model = models.Trabajador
        fields = '__all__'
        exclude = ('alta_usuario', 'alta_fecha')

class DocumentosPersonalesForm(FormControl):
    

    class Meta:
        model = models.DocumentosPersonales
        fields = '__all__'
        exclude = ('id_trabajador',)